import React, { Component } from 'react'
import Header from './Header'
import Banner from './Banner'
import Item from './Item'
import Footer from './Footer'
export default class LayoutBoostrap extends Component {
  render() {
    return (
      <div>
     <Header/>
 <body>
    <div className="container">
   <Banner/>

<div className="row">
   <div className="col 4">
    <Item/>
   </div>
   <div className="col 4">
    <Item/>
   </div>
   <div className="col 4">
    <Item/>
   </div>
</div>
<div className="row my-5 mb-3">
   <div className="col 4">
    <Item/>
   </div>
   <div className="col 4">
    <Item/>
   </div>
   <div className="col 4">
    <Item/>
   </div>
</div>

    </div>

  <Footer/>  
 </body>
      </div>
    )
  }
}
