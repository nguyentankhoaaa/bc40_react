import React, { Component } from 'react'
import style_Header from './LayoutCss/Header.module.css'
export default class Header extends Component {
  render() {
    return (
  <nav className={style_Header.nav}>
   <div className={style_Header.navmain}>
    <p>Start Boostrap</p>
   </div>
<div className={style_Header.navbar}>
    <a href="" className={style_Header.active}>Home</a>
    <a href="">About</a>
    <a href="">Contact</a>

</div>
  </nav>


    )
  }
}
