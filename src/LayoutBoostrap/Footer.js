import React, { Component } from 'react'
import style_Footer from './LayoutCss/Footer.module.css'
export default class Footer extends Component {
  render() {
    return (
     <div className={style_Footer.footer}>
     <p>Copyright © Your Website 2022</p>
     </div>
    )
  }
}
